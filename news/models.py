from django.db import models
from django.urls import reverse
from django.template.defaultfilters import slugify

class News(models.Model):
    category_id = models.ForeignKey('Category', on_delete=models.PROTECT, null=True, verbose_name='Category')
    title = models.CharField(max_length=150)
    slug = models.SlugField(max_length=150, unique=True, db_index=True, verbose_name='URL', null=True)
    content = models.TextField(blank=True)
    photo = models.ImageField(upload_to='photos/%Y/%m/%d/', blank=True)
    is_published = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('news:view_news', kwargs={'news_slug': self.slug})

    class Meta:
        verbose_name = 'New'
        verbose_name_plural = 'News'
        ordering = ['-created_at']

    def _generate_unique_slug(self):
        unique_slug = slugify(self.title)
        num = 1
        while News.objects.filter(slug=unique_slug).exists():
            unique_slug = '{}-{}'.format(unique_slug, num)
            num += 1
        return unique_slug

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = self._generate_unique_slug()
        super().save(*args, **kwargs)


class Category(models.Model):
    title = models.CharField(max_length=100, db_index=True)
    slug = models.SlugField(max_length=150, unique=True, db_index=True, verbose_name='URL', null=True)

    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('news:category', kwargs={'category_slug': self.slug})
