from django.urls import path
from news import views

app_name = 'news'
urlpatterns = [
    # path('', views.index, name='home'),
    path('', views.HomeNews.as_view(), name='home'),
    # path('category/<slug:category_slug>', views.get_category, name='category'),
    path('category/<slug:category_slug>', views.NewsByCategory.as_view(), name='category'),
    # path('news/<slug:news_slug>', views.view_news, name='view_news'),
    path('news/<slug:news_slug>', views.ViewNews.as_view(), name='view_news'),
    # path('news/add-news/', views.add_news, name='add_news'),
    path('news/add-news/', views.CreateNews.as_view(), name='add_news'),
    path('register/', views.register, name='register'),
    path('login/', views.user_login, name='login'),
    path('logout/', views.user_logout, name='logout'),
]

